<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class ImportUsersController extends AbstractController implements PasswordAuthenticatedInterface
{

    #[Route('/import/users', name: 'import_users')]
    public function index(): Response
    {
        return $this->render('import_users/index.html.twig', [
            'controller_name' => 'ImportUsersController',
        ]);
    }

   /**
     * @Route("/importusers", name="importUsers")
     */
    public function importUsers(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager): Response
    {

        $entityManager = $this->getDoctrine()->getManager();
        
        $connection = $entityManager->getConnection();
        $platform   = $connection->getDatabasePlatform();

        $connection->executeUpdate($platform->getTruncateTableSQL('user', true));
        
        $xml = file_get_contents("\Users\david\my_project\src\xml\users.xml");
        $xml=simplexml_load_string($xml) or die("Error: Cannot create object");
        foreach($xml->user as $item){

            $user = new User();
            $user->setEmail($item->email);
            $user->setPassword($encoder->encodePassword($user,$item->password));
    
            $entityManager->persist($user);
            $entityManager->flush();

        }

        return new RedirectResponse("/login");
    }

     /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function getPassword($credentials): ?string
    {
        return $credentials['password'];
    }


}
