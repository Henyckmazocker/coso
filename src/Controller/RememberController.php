<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


class RememberController extends AbstractController
{
    /**
     * @Route("/remember", name="remember")
     */
    public function remember(MailerInterface $mailer, EntityManagerInterface $entityManager, Request $request): Response
    {

        $data = $request->request->get('_inputEmail');
        $data2 = $request->request->get('_inputPassword');

        print($data."fasfsf");

        if($data == null or $data == ""){

        }else{

            $email = (new Email())
            ->from('david.carvajal.abellan@gmail.com')
            ->to($data)
            ->subject('recover password')
            ->text('recover password')
            ->html('<p>recover password</p>');

            $mailer->send($email);

        }

        if($data2 == null or $data2 == ""){

            return $this->render('remember/index.html.twig', ['last_username' => $data]);

        }else{

            $user = $entityManager->createQueryBuilder()
            ->select('u')
            ->from('user','u')
            ->where('u.email = :email')
            ->setParameter('email', $data)
            ->getQuery();

            $user->setPassword($this->passwordEncoder->encodePassword($user,$data2));
            $entityManager->persist($user);
            $entityManager->flush();

            return new RedirectResponse("/login");
        }
    }

    /**
     * @Route("/remembersubmit", name="remembersubmit")
     */
    public function remembersubmit(Request $request,EntityManagerInterface $entityManager,UserPasswordEncoderInterface $encoder): Response
    {

        print("asfas");

        $data = $request->request->get('email');
        $data2 = $request->request->get('password');

        print($data."fasfsf");

        if($data2 == null or $data2 == ""){

            return $this->render('remember/index.html.twig', ['last_username' => $data]);

        }else{

            $user = $entityManager->createQueryBuilder()
            ->select('user')
            ->from('App\Entity\User','user')
            ->where('user.email = :email')
            ->setParameter('email', $data)
            ->getQuery()
            ->getOneOrNullResult();

            $user->setPassword($encoder->encodePassword($user,$data2));
            $entityManager->persist($user);
            $entityManager->flush();

            return new RedirectResponse("/login");
        }
    }


}
